import { useEffect } from "react";
import { useState, useRef } from "react";

export default function SignUpToNewsLetter() {
  const [email, setEmail] = useState();

  const inputElement = useRef(null);

  const handleClick = () => {
    if (!email) {
      inputElement.current.style.border = "7px solid red";
      inputElement.current.focus();
    }
  };

  useEffect(() => {
    if (email) {
      inputElement.current.style.border = "none";
    }
  }, [email]);
  return (
    <>
      <div style={{ display: "flex", flexDirection: "row", gap: "8px" }}>
        <input
          ref={inputElement}
          type="text"
          placeholder="Email....."
          style={{
            paddingLeft: "8px",
            paddingRight: "8px",
            borderRadius: "8px",
          }}
          value={email || ""}
          onChange={(e) => setEmail(e.target.value)}
        />
        <button onClick={handleClick}>Sign Up</button>
      </div>
    </>
  );
}
