import { useState, useRef } from "react";

export default function MagicNumber() {
  const firstMagicNumber = 5;
  const secondMagicNumber = 10;

  const [messages, setMessages] = useState({
    p: "",
    button: "Click until the message show.",
  });

  console.log("render");

  const count = useRef(0);

  const handleClick = () => {
    count.current = count.current + 1;
    if (count.current === firstMagicNumber) {
      setMessages({
        p: "You Reach It!",
        button: "Click until the message show.",
      });
    } else if (count.current === secondMagicNumber) {
      setMessages({
        p: "You Reach It Twice!",
        button: "Click until the message show.",
      });
    }
  };
  return (
    <>
      {messages && <p>{messages.p}</p>}
      <button onClick={handleClick}>{messages.button}</button>
    </>
  );
}
