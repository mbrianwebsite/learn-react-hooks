import { useMemo, useState } from "react";

export default function MemoHook() {
  const [text, setText] = useState("");
  const [person, setPerson] = useState("");

  const isCool = () => {
    for (let i = 0; i < 1000000000; i++) {}
    if (person === "Aaroon") {
      return "Cool";
    }
    if (person === "Boy") {
      return "Maybe Cool";
    }
    if (person === "Charles") {
      return "Too Cool";
    }
  };

  const isCoolFunc = useMemo(isCool, [person]);
  return (
    <>
      <div style={{ display: "flex", flexDirection: "row", gap: "8px" }}>
        <input
          type="text"
          style={{
            paddingLeft: "8px",
            paddingRight: "8px",
            borderRadius: "8px",
          }}
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <div style={{ display: "flex", flexDirection: "column", gap: "8px" }}>
          <button onClick={() => setPerson("Aaroon")}>Aaroon</button>
          <button onClick={() => setPerson("Boy")}>Boy</button>
          <button onClick={() => setPerson("Charles")}>Charles</button>
        </div>
        {isCoolFunc}
      </div>
    </>
  );
}
